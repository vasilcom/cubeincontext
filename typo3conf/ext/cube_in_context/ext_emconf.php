<?php

/**
 * Extension Manager/Repository config file for ext "cube_in_context".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Cube In Context',
    'description' => 'Typo3 Package for CIC page',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-9.5.99',
            'fluid_styled_content' => '9.5.0-9.5.99',
            'rte_ckeditor' => '9.5.0-9.5.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'CubeInContext\\CubeInContext\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Vasili Macharadze',
    'author_email' => 'info@wasso.de',
    'author_company' => 'Cube In Context',
    'version' => '1.0.0',
];
