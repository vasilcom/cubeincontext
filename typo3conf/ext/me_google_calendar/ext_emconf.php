<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "me_google_calendar".
 *
 * Auto generated 12-08-2020 20:40
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Google Calendar',
  'description' => 'Includes the jQuery Plugin FullCalendar, which generates a skinable calendar with different views (month, week, day, week list, day list etc.) from Google Calendar XML Feed(s) - now with extbase/fluid.',
  'category' => 'plugin',
  'version' => '4.10.0',
  'state' => 'stable',
  'author' => 'Alexander Grein',
  'author_email' => 'alexander.grein@gmail.com',
  'author_company' => 'MEDIA::ESSENZ',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '9.5.0-10.4.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
  'autoload' => 
  array (
    'psr-4' => 
    array (
      'MediaEssenz\\MeGoogleCalendar\\' => 'Classes',
    ),
  ),
  'uploadfolder' => false,
  'createDirs' => NULL,
  'clearcacheonload' => false,
);

