<?php
declare(strict_types=1);

namespace MediaEssenz\MeGoogleCalendar\Utility;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use function unserialize;

/**
 * Configuration Utility.
 */
class ConfigurationUtility
{
  /**
   * Configuration cache.
   *
   * @var array
   */
  protected static $configuration;

  /**
   * Get the given configuration value.
   *
   * @param string $name
   *
   * @return string
   * @throws \TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException
   * @throws \TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException
   */
  public static function get($name): string
  {
      self::loadConfiguration();
      return isset(self::$configuration[$name]) ? self::$configuration[$name] : '';
  }

  /**
   * Load the current configuration.
   * @return void
   * @throws \TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException
   * @throws \TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException
   */
  protected static function loadConfiguration(): void
  {
    if (null === self::$configuration) {
      if (self::isTypo3OlderThen9()) {
        self::$configuration = (array)unserialize((string)$GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['me_google_calendar']);
      } else {
        self::$configuration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('me_google_calendar');
      }
    }
  }

  /**
   * Check if TYPO3 is older than 9
   *
   * @return bool
   */
  public static function isTypo3OlderThen9(): bool
  {
    return VersionNumberUtility::convertVersionNumberToInteger(TYPO3_version) < 9000000;
  }
}
