plugin.tx_megooglecalendar {
    view {
        # cat=plugin.tx_megooglecalendar/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:me_google_calendar/Resources/Private/Templates/
        # cat=plugin.tx_megooglecalendar/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:me_google_calendar/Resources/Private/Partials/
        # cat=plugin.tx_megooglecalendar/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:me_google_calendar/Resources/Private/Layouts/
    }

    persistence {
        # cat=plugin.tx_megooglecalendar//a; type=string; label=Storage Pids separated by comma; leave empty to ignore storage page
        storagePid =
        # cat=plugin.tx_megooglecalendar//a; type=int; label=Recursive level; only useful if storagePid is set
        recursive = 0
    }

    settings {
        # cat=plugin.megooglecalendar/file/10; type=string; label=Path to css file: Default - EXT:me_google_calendar/Resources/Public/Css/fullcalendar.css;
        cssPath = EXT:me_google_calendar/Resources/Public/Css/main.min.css

        # cat=plugin.megooglecalendar/file/15; type=string; label=Path to print css file: Default - EXT:me_google_calendar/Resources/Public/Css/fullcalendar.print.css;
        printCssPath = EXT:me_google_calendar/Resources/Public/Css/fullcalendar.print.min.css

        # cat=plugin.megooglecalendar/file/20; type=string; label=Path to theme css file: Default - EXT:me_google_calendar/Resources/Public/JavaScript/jquery-ui/themes/smoothness/jquery-ui.min.css;
        cssThemePath = EXT:me_google_calendar/Resources/Public/JavaScript/jquery-ui/themes/smoothness/jquery-ui.min.css

        # cat=plugin.megooglecalendar/file/30; type=string; label=Path to jQuery file: Default - EXT:me_google_calendar/Resources/Public/JavaScript/jquery/jquery.min.js;
        jQueryPath = EXT:me_google_calendar/Resources/Public/JavaScript/jquery/jquery.min.js

        # cat=plugin.megooglecalendar/file/40; type=string; label=Path to jQuery Ui file: Default - EXT:me_google_calendar/Resources/Public/JavaScript/jquery-ui/jquery-ui.min.js;
        jQueryUiPath = EXT:me_google_calendar/Resources/Public/JavaScript/jquery-ui/jquery-ui.min.js

        # cat=plugin.megooglecalendar/file/50; type=string; label=Path to fullcalendar.js file: Default - EXT:me_google_calendar/Resources/Public/JavaScript/fullcalendar.min.js;
        fullCalendarPath = EXT:me_google_calendar/Resources/Public/JavaScript/fullcalendar.min.js

        # cat=plugin.megooglecalendar/file/60; type=string; label=Path to jQuery plugin moment.min.js file: Default - EXT:me_google_calendar/Resources/Public/JavaScript/moment.min.js;
        fullCalendarMomentPath = EXT:me_google_calendar/Resources/Public/JavaScript/moment.min.js

        # cat=plugin.megooglecalendar/file/70; type=string; label=Path to jQuery plugin lang-all.js file: Default - EXT:me_google_calendar/Resources/Public/JavaScript/lang-all.js;
        fullCalendarLangAllPath = EXT:me_google_calendar/Resources/Public/JavaScript/locale-all.js

        # cat=plugin.megooglecalendar/file/80; type=string; label=Path to jQuery plugin gcal file: Default - EXT:me_google_calendar/Resources/Public/JavaScript/gcal.js;
        gcalPath = EXT:me_google_calendar/Resources/Public/JavaScript/gcal.min.js

        # cat=plugin.megooglecalendar/file/90; type=string; label=Path to jQuery calendar file: Default - EXT:me_google_calendar/Resources/Public/JavaScript/main.js;
        meGoogleCalendarPath = EXT:me_google_calendar/Resources/Public/JavaScript/main.min.js

        # cat=plugin.megooglecalendar/enable/10; type=boolean; label=jQuery external: The given path is an external url (starts with //);
        jQueryExternal = 0

        # cat=plugin.megooglecalendar/enable/20; type=boolean; label=Disable minify jQuery: If set (default), jQuery will *not* be minified;
        jQueryDisableCompression = 1

        # cat=plugin.megooglecalendar/enable/30; type=boolean; label=jQueryUi external: The given path is an external url (starts with //);
        jQueryUiExternal = 0

        # cat=plugin.megooglecalendar/enable/40; type=boolean; label=Disable minify jQueryUi: If set (default), jQueryUi will *not* be minified;
        jQueryUiDisableCompression = 1

        # cat=plugin.megooglecalendar/enable/50; type=boolean; label=Disable minify fullcalendar: If set, fullcalendar.js will *not* be minified;
        fullCalendarDisableCompression = 0

        # cat=plugin.megooglecalendar/enable/60; type=boolean; label=Disable minify gcal: If set, gcal.js will *not* be minified;
        gcalDisableCompression = 0

        # cat=plugin.megooglecalendar/enable/70; type=boolean; label=Disable minify me_google_calendar: If set, main.js will *not* be minified;
        meGoogleCalendarDisableCompression = 0

        # cat=plugin.megooglecalendar/enable/70; type=boolean; label=cssThemePath external: The given path is an external url (starts with //);
        cssThemePathExternal = 0

        # cat=plugin.megooglecalendar/enable/75; type=options[standard,bootstrap3,bootstrap4,jquery-ui]; label=themeSystem: Renders the calendar with a given theme system;
        themeSystem = standard

        # cat=plugin.megooglecalendar/enable/80; type=options[Footer=includeJSFooterlibs,Header=includeJSlibs]; label=jsLibsPosition: includeJSFooterlibs: Javascript libraries in footer / includeJSlibs: Javascript libraries in header;
        jsLibsPosition = includeJSFooterlibs

        # cat=plugin.megooglecalendar/enable/90; type=options[Footer=includeJSFooter,Header=includeJS]; label=jsPosition: includeJSFooter: Javascript in footer / includeJS: Javascript in header;
        jsPosition = includeJSFooter

        # cat=plugin.megooglecalendar/content/025; type=boolean; label=hide header content: Default - 0;
        hideHeader = 0

        # cat=plugin.megooglecalendar/content/030; type=string; label=Left header content: Default - prev,next today - Allowed - prev,next,today,title,month,agendaWeek,agendaDay,basicWeek,basicDay, listDay, listWeek, listMonth, listYear, list - Use [Space] for distance;
        headerLeft = prev,next today

        # cat=plugin.megooglecalendar/content/040; type=string; label=Center header content: Default - title - Allowed - prev,next,today,title,month,agendaWeek,agendaDay,basicWeek,basicDay, listDay, listWeek, listMonth, listYear, list - Use [Space] for distance;
        headerCenter = title

        # cat=plugin.megooglecalendar/content/050; type=string; label=Right header content: Default - month,agendaWeek,agendaDay - Allowed - prev, next, today, title, month, agendaWeek, agendaDay, basicWeek, basicDay, listDay, listWeek, listMonth, listYear, list - Use [Space] for distance;
        headerRight = month,agendaWeek,agendaDay

        # cat=plugin.megooglecalendar/content/051; type=boolean; label=hide footer content: Default - 1;
        hideFooter = 0

        # cat=plugin.megooglecalendar/content/030; type=string; label=Left footer content: Default - prev,next today - Allowed - prev,next,today,title,month,agendaWeek,agendaDay,basicWeek,basicDay, listDay, listWeek, listMonth, listYear, list - Use [Space] for distance;
        footerLeft = prev,next today

        # cat=plugin.megooglecalendar/content/040; type=string; label=Center footer content: Default - title - Allowed - prev,next,today,title,month,agendaWeek,agendaDay,basicWeek,basicDay, listDay, listWeek, listMonth, listYear, list - Use [Space] for distance;
        footerCenter = title

        # cat=plugin.megooglecalendar/content/050; type=string; label=Right footer content: Default - month,agendaWeek,agendaDay - Allowed - prev, next, today, title, month, agendaWeek, agendaDay, basicWeek, basicDay, listDay, listWeek, listMonth, listYear, list - Use [Space] for distance;
        footerRight = month,agendaWeek,agendaDay

        # cat=plugin.megooglecalendar/content/060; type=options[month,agendaWeek,agendaDay,basicWeek,basicDay,listDay,listWeek,listMonth,listYear,list]; label=Default view: Default - agendaWeek - Allowed - month,agendaWeek,agendaDay,basicWeek,basicDay;
        defaultView = agendaWeek

        # cat=plugin.megooglecalendar/content/070; type=boolean; label=Show all day slot;
        allDaySlot = 0

        # cat=plugin.megooglecalendar/content/080; type=options[Monday=1, Sunday=0]; label=First day of week;
        firstDay = 0

        # cat=plugin.megooglecalendar/content/090; type=boolean; label=Show weekends;
        weekends = 0

        # cat=plugin.megooglecalendar/content/092; type=boolean; label=Show week numbers;
        weekNumbers = 0

        # cat=plugin.megooglecalendar/content/094; type=boolean; label=Show week numbers within days;
        weekNumbersWithinDays = 0

        # cat=plugin.megooglecalendar/content/096; type=options[ISO=ISO,local=local]; label=week number calculation;
        weekNumberCalculation = ISO

        # cat=plugin.megooglecalendar/content/100; type=string; label=Min time: Default - 0;
        minTime = 00:00:00

        # cat=plugin.megooglecalendar/content/110; type=string; label=Max time: Default - 24;
        maxTime = 24:00:00

        # cat=plugin.megooglecalendar/content/120; type=string; label=Scroll to time;
        scrollTime = 08:00:00

        # cat=plugin.megooglecalendar/content/135; type=boolean; label=hide title content: Default - 0;
        hideTitle = 0

        # cat=plugin.megooglecalendar/content/138; type=boolean; label=Enable event limit: Show more events link, if they do not fit into time slot;
        eventLimit = 0

        # cat=plugin.megooglecalendar/content/140; type=string; label=Title format month: Default - MMMM YYYY;
        titleFormat.month = MMMM YYYY

        # cat=plugin.megooglecalendar/content/150; type=string; label=Title format week: Default - MMM D YYYY;
        titleFormat.week = MMM D YYYY

        # cat=plugin.megooglecalendar/content/160; type=string; label=Title format day: Default - MMMM D YYYY;
        titleFormat.day = MMMM D YYYY

        # cat=plugin.megooglecalendar/content/170; type=string; label=Column format month: Default - dddd;
        columnFormat.month = dddd

        # cat=plugin.megooglecalendar/content/180; type=string; label=Column format week: Default - ddd DD.MM;
        columnFormat.week = ddd DD.MM.

        # cat=plugin.megooglecalendar/content/190; type=string; label=Column format day: Default - dddd DD.MM;
        columnFormat.day = dddd DD.MM.

        # cat=plugin.megooglecalendar/content/192; type=string; label=Column format day: Default - dddd DD.MM;
        listDayFormat = DD. MMMM YYYY

        # cat=plugin.megooglecalendar/content/194; type=string; label=Column format day: Default - dddd DD.MM;
        listDayAltFormat = dddd

        # cat=plugin.megooglecalendar/content/200; type=string; label=Time format agenda: Default - HH:mm;
        timeFormat.agenda = HH:mm

        # cat=plugin.megooglecalendar/content/202; type=string; label=Time format list: Default - HH:mm;
        timeFormat.list = HH:mm

        # cat=plugin.megooglecalendar/content/210; type=string; label=Time format general: Default - HH:mm;
        timeFormat.general = HH:mm

        # cat=plugin.megooglecalendar/content/211; type=string; label=Time format month: Default - HH:mm;
        timeFormat.month = HH:mm

        # cat=plugin.megooglecalendar/content/212; type=string; label=Time format week: Default - HH:mm;
        timeFormat.week = HH:mm

        # cat=plugin.megooglecalendar/content/213; type=string; label=Time format day: Default - HH:mm;
        timeFormat.day = HH:mm

        # cat=plugin.megooglecalendar/content/225; type=string; label=Time Zone: Format e.g. Europe/Berlin. If empty (default) the timezone defined inside google UI will be used;
        timeZone =

        # cat=plugin.megooglecalendar/content/230; type=boolean; label=Do not generate link to google maps: Default - 0;
        noGoogleMapsLink = 0

        # cat=plugin.megooglecalendar/content/240; type=boolean; label=Hide ical download button in event details: Default - 0;
        hideIcalDownloadButton = 0

        # cat=plugin.megooglecalendar/content/250; type=boolean; label=Hide add to google calendar button: Default - 0;
        hideAddtoGoogleCalendarButton = 0

        # cat=plugin.megooglecalendar/dims/260; type=int+; label=Height of calendar: If empty, height will follow default aspectRatio 1.35;
        height = 0

        # cat=plugin.megooglecalendar/dims/270; type=string; label=Determines the width-to-height aspect ratio of the calendar: larger numbers make smaller heights;
        aspectRatio = 1.35

        # cat=plugin.megooglecalendar/general/10; type=string; label=override flexform setting if empty;
        overrideFlexformSettingsIfEmpty = cssThemePath,headerLeft,headerCenter,headerRight,footerLeft,footerCenter,footerRight,defaultView,allDaySlot,firstDay,firstHour,weekends,minTime,maxTime,hideTitle,hideIcalDownloadButton,hideAddtoGoogleCalendarButton,noGoogleMapsLink,height

        # cat=plugin.megooglecalendar/general/12; type=string; label=eID Script URL for ics file generation;
        eIdGetIcsUrl = /index.php?eID=meGoogleCalendarEidGetIcs

        # cat=plugin.megooglecalendar/general/13; type=string; label=language key;
        language = de
    }
}