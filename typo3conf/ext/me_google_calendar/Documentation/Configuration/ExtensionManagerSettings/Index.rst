﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../../Includes.txt

Extension Manager Settings
^^^^^^^^^^^^^^^^^^^^^^^^^^


.. ### BEGIN~OF~TABLE ###

.. container:: table-row

   Property
         restrictToPredefinedCssClasses

   Data type
         boolean

   Description
         Restrict google calendar record css to predefined styles. See manual for more info.

   Default
         0


.. ###### END~OF~TABLE ######
